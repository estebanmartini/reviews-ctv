require 'grape'
require 'sinatra'
require 'rack/cors'
require 'hashie-forbidden_attributes'
require 'uri'

ENV['ENV'] ||= ENV['RACK_ENV']

require(File.expand_path('lib/reviews_classroomtv', __dir__ ))
require(File.expand_path('apps/api', __dir__ ))
require(File.expand_path('apps/user', __dir__ ))
require(File.expand_path('apps/admin', __dir__ ))

run Rack::URLMap.new ({
  '/' => Apps::User,
  '/api' => Apps::API,
  '/admin' => Apps::Admin
})