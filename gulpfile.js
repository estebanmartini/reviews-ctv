'use strict';

var gulp      = require('gulp');
var sass      = require('gulp-sass');
var minifyCSS = require('gulp-minify-css');

var sassDir = {
    source: './sass/**/*.scss',
    output: './public/css'
};

gulp.task('sass', function () {
    gulp.src(sassDir.source)
        .pipe(sass(sassDir.source).on('error', sass.logError))
        .pipe(minifyCSS())
        .pipe(gulp.dest(sassDir.output));
});

gulp.task('sass:watch', function () {
    gulp.watch(sassDir.source, ['sass']);
});

gulp.task('default', function() {

});