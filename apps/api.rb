module Apps

  class API < Grape::API

    version 'v1', using: :path, vendor: 'Classroom.tv'

    format :json
    rescue_from :all

    rescue_from Grape::Exceptions::ValidationErrors do |e|
      error_response ({message: e.message, status: 400})
    end

    helpers do

    end

    resource :ratings do

      desc "Return a rating"
      params do
        requires :id, type: String, desc: "The Rating id"
      end
      route_param :id do
        get do
          rating = Models::Rating.where(:id => params[:id]).first
          if rating.nil?
            error! "The Rating doesn't exists", 404
          else
            rating
          end
        end

        desc "updates a rating"
        params do
          requires :rating, type: Hash do
            optional :explicit_rating, type: Integer
          end
        end
        put do
          rating = Models::Rating.find(params[:id])
          if rating.update(declared(params)[:rating])
            rating.update_ctv_course_ratings
            rating
          else
            error! rating.errors, 400
          end
        end

        desc "creates a new rating´s review"
        params do
          requires :review, type: Hash do
            requires :title, type: String
            requires :body,  type: String
          end
        end
        post :reviews do

          review = Models::CourseReview.new(declared(params)[:review])
          review.rating_id = params[:id]

          if review.save && review.errors.empty?
            review
          else
            error! review.attributes, 400
          end
        end
      end

      desc "Creates a new rating"
      params do
        requires :rating, type: Hash do
          requires :course_id, type: Integer
          requires :user_id, type: Integer
          requires :explicit_rating, type: Integer
        end
      end
      post do
        rating = Models::Rating.create(declared(params)[:rating])
        if rating.errors.empty?
          rating.reload
          rating.update_ctv_course_ratings
          rating
        else
          error! rating.errors, 400
        end
      end
    end

    resource :reviews do
      params do
        requires :id, type: String, desc: 'The Course Review id'
      end
      route_param :id do
        desc 'Updates a review'
        params do
          requires :review, type: Hash do
            requires :title, type: String
            requires :body,  type: String
            requires :rating_id, type: String
          end
        end
        put do
          review = Models::CourseReview.where(:id => params[:id]).first
          error! 'The Review does not exist', 404 if review.nil?

          if review.update(declared(params)[:review])
            review
          else
            error! review.errors, 400
          end
        end

        params do
          requires :review_feedback, type: Hash do
            requires :user_id, type: Integer
            requires :feedback, type: Boolean
            requires :course_review_id, type: String
          end
        end

        post :feedback do
          review = Models::CourseReview.where(:id => params[:id]).first

          unless review.nil?

            review_feedback_params = declared(params)[:review_feedback]

            model = Models::CourseReviewFeedback.where(
                :user_id => review_feedback_params.user_id,
                :course_review_id => params[:id]
            ).first

            unless model.nil?
              model.feedback = review_feedback_params.feedback
            else
              model = Models::CourseReviewFeedback.new(review_feedback_params)
            end

            if model.save
              {
                feedback: {
                    positive: model.course_review.positive_reviews_feedback.count,
                    negative: model.course_review.negative_reviews_feedback.count
                }
              }

            else
              error! model.errors, 400
            end

          end

        end
      end
    end

    resource :courses do
      desc "Gets the course ratings"
      params do
        requires :user_id, type: Integer
      end
      get :ratings do
        ratings =Models::Rating.eager_load(:course, :course_review).course_ratings.where(:user_id => params[:user_id]).order_by(:original_value => :desc)
        output = []

        ratings.each do |r|

          course = r.course.as_json
          course[:rating] = r.as_json

          unless r.course_review.nil?
            course[:rating][:review] = r.course_review.as_json
          end

          output << course
        end

        output
      end

      #/courses/:id
      params do
        requires :id, type: Integer, desc: 'The course id'
      end

      route_param :id do

        get do

          ratings = Models::Rating.courses_with_explicit_rating.where(:course_id => params[:id]).pluck(:id, :course_id, :explicit_rating)

          {
            course: Models::Course.where(:course_id => params[:id]).first,
            ratings: ratings,
            stars_count: NoBrainer.run {ratings.to_rql.group('explicit_rating').count}
          }

        end

        get :reviews do
          ratings = Models::Rating.with_explicit_rating.where(:course_id => params[:id])
          reviews = []
          ratings.eager_load(:course_review, :user).each do |r|
            unless r.course_review.nil?
              reviews << {
                id: r.course_review.id,
                title: r.course_review.title,
                body: r.course_review.body,
                rating_id: r.id,
                rating: r,
                author: {
                    user_id: r.user_id,
                    hi_name: r.user.hi_name,
                    avatar_url: "#{App.params['web']['user_profile_avatar_url']}/#{r.user_id}"
                },
                updated_at: r.course_review.updated_at,
                feedback: {
                    positive: r.course_review.positive_reviews_feedback.count,
                    negative: r.course_review.negative_reviews_feedback.count
                }
              }
            end
          end

          {
              reviews: reviews
          }

        end

      end

    end

    resource :users do
      params do
        requires :id, type: Integer, desc: 'The User id'
      end

      route_param :id do
        get do
          Models::User.where(:user_id => params[:id]).first
        end

        #/user/:id/ratings
        params do
          optional :course_id, type: Integer
        end
        get :ratings do
          result = {}
          unless params[:course_id].nil?
            rating = Models::Rating.course_ratings.where(:course_id=>params[:course_id], :user_id=>params[:id]).first

            result = {
              course_rating: rating.nil? ? {} : rating,
              review: rating.nil? ? {} : rating.course_review,
              params: params
            }

          end

          result
        end
      end

      route_param :id do
        desc 'Updates the user subscriber state'
        params do
          requires :user, type: Hash do
            requires :is_email_subscriber, type: Boolean
          end
        end

        put do
          user = Models::User.where(:user_id => params[:id]).first
          error! 'The User does not exist', 404 if user.nil?

          if user.update(declared(params)[:user])
            user
          else
            error! declared(params)[:user], 400
          end

        end
      end
    end


    resource :recommendations do

      params do
        requires :user_id, type: Integer, desc: 'The User id'
        requires :items, type: String, desc: 'The Items type (courses or lectures)'
        requires :rating_type, type: String, desc: 'explicit or implicit'
        optional :limit, type: Integer, desc: 'Number of items to retrieve'
      end

      get do

        rec_set = Models::RecommendationSet.where(:user_id => params[:user_id])

        if params[:items] == 'courses'
            rec_set = rec_set.with_course_type
        elsif params[:items] == 'lectures'
            rec_set = rec_set.with_lecture_type
        else
          error! '', 400
        end

        if params[:rating_type] == 'implicit'
          rec_set = rec_set.with_implicit_rating
        else
          rec_set = rec_set.with_explicit_rating
        end

        if rec_set.first.nil?
          nil
        else
          rec_set.first.recommended_not_rated_items(params[:limit] || 100)
        end

      end

    end

  end
end