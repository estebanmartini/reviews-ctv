module Apps
  class User < Sinatra::Base

    set :root, File.dirname(__FILE__) + '/user'
    set :public_folder, File.dirname(__FILE__) + '/../public'
    set :environment,  ENV['ENV'] ? ENV['ENV'].to_sym : :development
    disable :run, :reload
    set :protection, :except => :frame_options

    error 400 do
      'No sabemos qué hacer con esta URL. Por favor revísala e intentalo nuevamente'
    end

    get '/courses/reviews/:auth_token' do

      user_id = Models::User.decrypt(Base64.urlsafe_decode64(params[:auth_token])) rescue nil

      halt 400 if user_id.nil?

      erb :'courses/reviews/index.html',
          :layout => :'layouts/default',
          :locals => {
              :page_title => 'Califica tus cursos',
              :user => Models::User.where(:user_id => user_id).first,
              :angularjs => {
                  :app_name => 'course_reviews'
              },
              :scripts => [
                  '/js/apps/users/courses/reviews/main.js'
              ]
          }
    end

    get '/emails/unsubscribe/:auth_token' do
      user_id = Models::User.decrypt(Base64.urlsafe_decode64(params[:auth_token])) rescue nil

      halt 400 if user_id.nil?

      erb :'emails/unsubscribe.html',
        :layout => :'layouts/default',
        :user => Models::User.where(:user_id => user_id).first,
        :locals => {
          :page_title => 'Cancelar suscripción',
          :user => Models::User.where(:user_id => user_id).first,
          :angularjs => {
              :app_name => 'email_subscription'
          },
          :scripts => [
              '/js/apps/users/emails/main.js'
          ]
        }
    end

    get '/review' do

      erb :'courses/review.html',
        :layout => :'layouts/basic.html',
        :locals => {
          :page_title => 'Comentarios de curso',
          :angularjs => {
              :app_name => 'courseView'
          },
        }
    end


  end
end