module Apps
  class Admin < Sinatra::Base

    set :root, File.dirname(__FILE__) + '/admin'
    set :public_folder, File.dirname(__FILE__) + '/../public'
    set :environment,  ENV['ENV'] ? ENV['ENV'].to_sym : :development
    disable :run, :reload

    ##
    # Not really secure, SSL must be set on the production environment
    #
    use Rack::Auth::Basic, "Protected Area" do |username, password|
      username == ENV['ADMIN_USER'] && password == ENV['ADMIN_PASS']
    end

    get '' do

      erb :'/main/index.html',
        :layout => :'layouts/default.html',
        :locals => {
            :page_title => 'Admin'
        }
    end

    get '/lectures/implicit_rating' do

      page = params[:page].to_i
      items_per_page = 500
      offset = items_per_page*page

      lectures = Models::Lecture.all.order_by(:lecture_id).limit(items_per_page).offset(offset).eager_load(:ratings)

      criteria = Models::Rating.lecture_ratings.with_implicit_rating

      erb :'/lectures/index.html',
        :layout => :'layouts/default.html',
        :locals => {
          :page_title => 'Clases',
          :lectures => lectures,
          :lectures_count => Models::Lecture.all.count,
          :lecture_ratings_count => criteria.count,
          :unique_lectures => NoBrainer.run {criteria.to_rql.group('lecture_id').count}.count,
          :unique_users => NoBrainer.run {criteria.to_rql.group('user_id').count}.count,
          :current_page => page,
          :items_per_page => items_per_page
        }

    end

    get '/courses' do
      erb :'/courses/index.html',
        :layout => :'layouts/default.html',
        :locals => {
          :page_title => 'Cursos',
          :courses => Models::Course.all.order_by(:average_rating => :desc),
          :course_reviews_count => Models::Course.reviews_count,
          :stats => Models::Rating.courses_stats,
          :implicit_ratings => Models::Course.implicit_ratings_count
        }
    end

    get '/courses/:filter' do

      if params[:filter] == 'explicit_rating'
        erb :'/courses/index_explicit.html',
          :layout => :'layouts/default.html',
          :locals => {
            :page_title => 'Dashboard',
            :ratings => Models::Rating.course_ratings.with_explicit_rating.order_by(:user_id).eager_load(:user,:course,:course_review),
            :stats => Models::Rating.courses_stats,
            :angularjs => {:app_name => ''},
            :scripts => [],
          }
      else
        halt 400
      end

    end

    get '/recommendations/:items/:rating_type' do

      page = params[:page].to_i
      items_per_page = 100
      offset = items_per_page*page

      rec_sets = Models::RecommendationSet.with_explicit_rating

      if params[:rating_type] == 'implicit'
        rec_sets = Models::RecommendationSet.with_implicit_rating
      end

      if params[:items] == 'courses'
        rec_sets = rec_sets.with_course_type
      else
        rec_sets = rec_sets.with_lecture_type
      end

      erb :"/recommendations/#{params[:items]}.html",
        :layout => :'layouts/default.html',
        :locals => {
          :page_title => 'Recomendaciones de ' + params[:items] == 'courses' ? 'curso' : 'clases',
          :rec_sets   => rec_sets.offset(offset).limit(items_per_page),
          :rec_sets_count => rec_sets.count,
          :current_page => page,
          :items_per_page => items_per_page
        }
    end

    get '/recommendations/:items/:rating_type/:id' do

      rec_set = Models::RecommendationSet.where(:id=>params[:id]).first
      ratings = Models::Rating.where(:user_id =>rec_set.user_id)
      user_ratings = {}

      ratings = params[:items] == 'courses' ? ratings.course_ratings : ratings.lecture_ratings

      if params[:rating_type] == 'implicit'
        ratings.with_implicit_rating.each do |r|
          user_ratings[(r.course_id || r.lecture_id).to_s] = {
              visto: r.implicit_rating,
              suscrito: r.is_enrolled_to_course ? 1 : 0
          }
      end
      else
        ratings.with_explicit_rating.each do |r|
          user_ratings[(r.course_id || r.lecture_id).to_s] = r.explicit_rating
        end
      end

      erb :"/recommendations/#{params[:items]}_view.html",
        :layout => :'layouts/default.html',
        :locals => {
          :page_title => 'Recomendaciones de curso',
          :rec_set => rec_set,
          :user_ratings => user_ratings
        }
    end

  end
end