module Classroomtv

  @config = {
    mysql: nil,
    email: nil
  }

  def self.setup_mysql_connection
    
    if @config[:mysql].nil?
      mysql_config_file = File.join(File.expand_path('../config/mysql.yml',__dir__))
      env = ENV['ENV'] || 'development'
      @config[:mysql] = YAML.load(ERB.new(File.read(mysql_config_file)).result)[env]
    end

    @config[:mysql]
  end

  ##
  # Gets a new <i>Mysql2::Client</i> object
  def self.mysql_client
    Mysql2::Client.new(Classroomtv::setup_mysql_connection)
  end

  ##
  # Email config
  def self.email_config

    if @config[:email].nil?
      email_config_file = File.join(File.expand_path('../config/email.yml',__dir__))
      env = ENV['ENV'] || 'development'
      @config[:email] = YAML.load(ERB.new(File.read(email_config_file)).result)[env]
    end

    @config[:email]
  end

end