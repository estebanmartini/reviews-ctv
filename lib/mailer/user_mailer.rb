##
# Intended to ask for ratings to users and send recommended courses
#
class UserMailer < Mailer

  def self.send_all_rate_courses(is_test = false)
    users = {}
    mailer = self.new
    results = {:sent => 0, :failed => 0, :unsubscribed => 0}

    Models::Rating.course_ratings.with_implicit_rating.eager_load(:user).order_by(:user_id).each do |r|
      users[r.user_id] = r.user if users[r.user_id].nil?
    end

    # exclude users who already rated items
    Models::Rating.course_ratings.with_explicit_rating.each do |r|
      users.delete(r.user_id) unless users[r.user_id].nil?
    end

    users.each do |user_id, user|
      p user.email
      begin
        if user.email_subscriber?
          mailer.rate_courses(user_id, user.email, is_test)
          results[:sent] += 1
        else
          results[:unsubscribed] += 1
        end

      rescue Exception => e
        pp e
        results[:failed] += 1
      end
    end

    results

  end


  ##
  # Sends and email to each user who has a course recommended set based on
  # implicit ratings.
  #
  def self.send_all_recommended_courses(is_test = false)

    mailer = self.new
    results = {:sent => 0, :failed => 0, :unsubscribed => 0}

    Models::RecommendationSet.with_implicit_rating.with_course_type.eager_load(:user).each do |rec_set|

      user = rec_set.user
      p user.email

      begin
        if user.email_subscriber?
          mailer.recommended_courses(user.user_id, user.email, is_test)
        end
      rescue Exception => e
        pp e
        results[:failed] += 1
      end

    end
  end

  ##
  # Sends an email asking to the user to rate their previously watched courses
  #
  def rate_courses(user_id, user_email=nil, is_test=false)

    # get the user ratings:
    ratings = Models::Rating.eager_load(:course).course_ratings
                  .where(:user_id => user_id).order_by(:original_value => :desc).limit(4)

    user = nil
    courses = []

    ratings.each do |r|
      user ||= r.user
      courses << r.course
    end

    if user_email.nil?
      user_email = user.email
    end

    html_view = 'user_mailer/rate_courses.html.erb'
    text_view = 'user_mailer/rate_courses.text.erb'

    encrypted_user_id = Base64.urlsafe_encode64(Models::User.encrypt(user_id))
    course_reviews_url = App.params['web']['course_reviews_url'] +'/'+encrypted_user_id
    unsubscribe_url = App.params['web']['unsubscribe_url'] +'/'+encrypted_user_id

    email_data = {
        :user => user,
        :courses => courses,
        :reviews_url => course_reviews_url,
        :unsubscribe_url => unsubscribe_url
    }

    html_email = render(html_view, email_data)
    text_email = render(text_view, email_data)

    params = {
        :to => user_email,
        :subject => 'Invitación a calificar tus cursos',
    }

    text_part = Mail::Part.new do
      body text_email
      content_type 'text/html; charset=UTF-8'
    end

    html_part = Mail::Part.new do
      body html_email
      content_type 'text/html; charset=UTF-8'
    end

    @email.text_part = text_part
    @email.html_part = html_part

    File.write(File.expand_path("../../logs/emails/#{user_id}.html", __dir__), html_email)

    deliver(params) unless is_test
  end

  ##
  #
  #
  def recommended_courses(user_id, user_email=nil, is_test=false)

    # get the user ratings:
    rec_set = Models::RecommendationSet.with_course_type.with_implicit_rating.where(:user_id => user_id).last
    ratings = Models::Rating.course_ratings.with_implicit_rating.where(:user_id => user_id).all
    rec_items = Models::RecommendedCourse.where(:recommendation_set_id => rec_set.id)

    user = Models::User.where(:user_id => user_id).first
    encrypted_user_id = Base64.urlsafe_encode64(Models::User.encrypt(user_id))
    unsubscribe_url = App.params['web']['unsubscribe_url'] + '/' + encrypted_user_id

    course_ids = ratings.inject({}){|ids, r| ids[r.course_id] = r.course_id; ids}
    courses = []
    stars = {}

    if user_email.nil?
      user_email = user.email
    end

    rec_items.each do |item|
      if course_ids[item.course_id.to_i].nil?
        courses << item.course
        stars[item.course_id.to_i] = item.course.average_rating || 0
      end
    end

    html_view = 'user_mailer/recommended_courses.html.erb'
    text_view = 'user_mailer/recommended_courses.text.erb'

    email_data = {
        :user => user,
        :stars => stars,
        :courses => courses,
        :unsubscribe_url => unsubscribe_url,
    }

    html_email = render(html_view, email_data)
    text_email = render(text_view, email_data)

    params = {
        :to => user_email,
        :subject => 'Cursos recomendados para ti',
    }

    text_part = Mail::Part.new do
      body text_email
      content_type 'text/html; charset=UTF-8'
    end

    html_part = Mail::Part.new do
      body html_email
      content_type 'text/html; charset=UTF-8'
    end

    @email.text_part = text_part
    @email.html_part = html_part

    File.write(File.expand_path("../../logs/emails/rec_courses_#{user_id}.html", __dir__), html_email)

    deliver(params) unless is_test
  end

end