require 'erb'
require 'json'
require 'yaml'
require 'nobrainer'
require 'mysql2'
require 'base64'
require 'openssl'
require 'mail'
require 'net/http'
require "uri"
require 'predictionio'

#initializers:
require(File.expand_path('../config/initializers/nobrainer', __dir__ ))

#models
require(File.expand_path('models', __dir__ ))
require(File.expand_path('models/user', __dir__ ))
require(File.expand_path('models/course', __dir__ ))
require(File.expand_path('models/lecture', __dir__ ))
require(File.expand_path('models/rating', __dir__ ))
require(File.expand_path('models/course_review', __dir__ ))
require(File.expand_path('models/course_review_feedback', __dir__ ))
require(File.expand_path('models/recommendation_set', __dir__ ))
require(File.expand_path('models/recommended_item', __dir__ ))
require(File.expand_path('models/recommended_course', __dir__ ))
require(File.expand_path('models/recommended_lecture', __dir__ ))

#mysql
require(File.expand_path('classroomtv', __dir__ ))

#mailer
require(File.expand_path('mailer', __dir__ ))
require(File.expand_path('mailer/user_mailer', __dir__ ))

#reviews
require(File.expand_path('reviews/recommender_system', __dir__ ))

module App

  @available_params = [
      'web'
  ]

  @app_params = {}

  ##
  # Returns a <code>hash</code> with the App parameters
  #   App.parms   #=> {'web' => {}}
  #
  def self.params

    if @app_params.empty?
      env = ENV['ENV'] || 'development'
      @available_params.each do |param|
        if @app_params[param].nil?
          config_file = File.join(File.expand_path('../config/'+param+'.yml',__dir__))
          @app_params[param] = YAML.load(ERB.new(File.read(config_file)).result)[env] if File.exist?(config_file)
        end
      end
    end

    @app_params
  end

end

