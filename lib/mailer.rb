class Mailer
  include ERB::Util

  @email = nil

  def initialize
    @email = email_client
    @email.from self.from('reviews')
    @views_dir = File.expand_path('../mailers/views/', __dir__ )
  end

  def views_dir
    @views_dir
  end

  def render(view, locals = {})
    b = binding
    locals.each{|k,v| b.local_variable_set(k.to_s, v)}

    view_file = File.read(File.join(@views_dir, view)).gsub(/\n?/, "")

    ERB.new(view_file).result(b)
  end

  ##
  #
  def deliver(params)
    params.each { |k,v|  @email[k] = v}
    @email.deliver
  end

  ##
  # Creates a name@classroom.tv address in a RFC 5322 compliant way
  #
  # Examples:
  # name 'esteban' #=> 'esteban@classroom.tv'
  def from name
    email = "#{name}@" + 'classroom.tv'

    "\"Classroom.tv\" <#{email}>"
  end

  private
  ##
  # Gets a pre-configured Mail object
  def email_client
    mail = Mail.new
    mail.delivery_method :smtp, Classroomtv::email_config.symbolize_keys

    mail
  end

end