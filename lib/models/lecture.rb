module Models

  ##
  # This class represents the ClassroomTV Lecture model, it is used as local copy to decouple the systems,
  # all its entries must exist in the Classroom.tv database and should not be created manually
  #

  class Lecture
    include NoBrainer::Document
    include NoBrainer::Document::Timestamps
    store_in :table => 'lectures'

    field :lecture_id,  :type => Integer, :required => true, :primary_key => true, :unique => true, :index => true
    field :title,       :type => String
    field :view_count,  :type => Integer

    # Relations
    has_many :ratings,  :foreign_key => :lecture_id
    belongs_to :course, :foreign_key => :course_id

    ##
    # Creates or updates the Lectures models from the Classroom.tv ctv_class table.

    def self.load_lectures_from_classroomtv

      log = {
          total:   0,
          created: 0,
          updated: 0,
          failed:  0
      }

      documents = []
      query = 'SELECT c.id lecture_id, c.title, c.view_count, c.course_id FROM view_spa_lectures c'

      Classroomtv::mysql_client.query(query, :stream => true, :symbolize_keys => true).each do |row|

        lecture = Models::Lecture.where(:lecture_id => row[:lecture_id]).first

        if lecture.nil?
          row[:created_at] = Time.now
          row[:updated_at] = Time.now
          if documents.push(row).count == 200
            log[:created] += Models::Lecture.insert_all(documents).count
            documents = []
          end
        else
          row.delete(:lecture_id)
          lecture.assign_attributes(row)
          if lecture.changed?
            log[lecture.save ? :updated : :failed] += 1
          end
        end

        log[:total] += 1

      end

      log[:created] += Models::Lecture.insert_all(documents).size unless documents.empty?

      log

    end

  end
end