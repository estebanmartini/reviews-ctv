module Models

  class RecommendedLecture < RecommendedItem
    belongs_to :lecture,   :foreign_key => :lecture_id
  end

end
