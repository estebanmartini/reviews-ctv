module Models
  class CourseReview

    include NoBrainer::Document
    include NoBrainer::Document::Timestamps

    store_in :table => 'course_reviews'

    field :title,  :type => String, :required => true
    field :body,   :type => Text,   :required => true

    # Relations
    belongs_to :rating, :class_name => 'Rating', :index => true, :required => true
    has_many :course_reviews

    has_many :positive_reviews_feedback, :class_name => 'CourseReviewFeedback', :scope => ->{ positive_feedback }
    has_many :negative_reviews_feedback, :class_name => 'CourseReviewFeedback', :scope => ->{ negative_feedback }

  end
end