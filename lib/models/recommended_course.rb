module Models

  class RecommendedCourse < RecommendedItem
    belongs_to :course,   :foreign_key => :course_id
  end

end
