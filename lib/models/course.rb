module Models
  ##
  # This class represents the ClassroomTV Course model, it is used as local copy to decouple the systems,
  # all its entries must exist in the Classroom.tv database and should not be created manually
  #

  class Course
    include NoBrainer::Document
    include NoBrainer::Document::Timestamps
    store_in :table => 'courses'

    ## fields:
    field :course_id,   :type => Integer, :required => true, :primary_key => true, :unique => true
    field :title,       :type => String
    field :image_url,   :type => String
    field :view_count,  :type => Integer
    field :average_rating, :type => Float   # the average rating
    field :explicit_ratings_count, :type => Integer # the number of users whose rated the course

    # indexes
    index :course_id

    ## relationships
    has_many :ratings,  :class_name => 'Rating',  :foreign_key => :course_id
    has_many :lectures, :class_name => 'Lecture', :foreign_key => :course_id
    has_many :course_reviews,  :through => :ratings

    ##
    # alias for :course_reviews relationship
    def reviews
      course_reviews
    end

    ##
    # Creates or updates the Courses models from the Classroom.tv ctv_course and related tables.

    def self.load_courses_from_classroomtv

      log = {
          total:   0,
          created: 0,
          updated: 0,
          failed:  0
      }

      documents = []
      query = 'SELECT id course_id, title, view_count, image_url FROM view_spa_courses'

      Classroomtv::mysql_client.query(query, :stream => true, :symbolize_keys => true).each do |row|

        course = Models::Course.where(:course_id => row[:course_id]).first

        if course.nil?
          row[:created_at] = Time.now
          row[:updated_at] = Time.now

          if documents.push(row).size == 200
            log[:created] += Models::Course.insert_all(documents).count
            documents = []
          end
        else
          row.delete(:course_id)
          course.assign_attributes(row)
          if course.changed?
            log[course.save() ? :updated : :failed] += 1
          end
        end

        log[:total] += 1

      end

      log[:created] += Models::Course.insert_all(documents).size unless documents.empty?


      log

    end

    ##
    #
    #
    def self.update_aggregated_fields(course_id)

      criteria = Rating.course_ratings.with_explicit_rating.where(:course_id => course_id)
      average_rating = criteria.avg(:explicit_rating)
      explicit_ratings_count = criteria.count

      Course.where(:course_id => course_id).first.update(
          :average_rating => average_rating,
          :explicit_ratings_count => explicit_ratings_count
      )
    end

    ##
    # Gets the total of reviews of each course
    #
    # Returns a Hash where each key is a course_id and its value is the
    # reviews count
    #
    def self.reviews_count
      courses = {}
      rql2 = CourseReview.rql_table.eq_join('rating_id', Rating.rql_table).zip.group('course_id')

      NoBrainer.run { rql2.count }.each {|data| courses[data[0]] = data[1]}

      courses
    end

    ##
    #
    # Returns a Hash where each key is a course_id and its value is the
    # implicit ratings count. It only considered those courses with implicit rating
    #
    # Examples:
    # Models::Course.implicit_ratings_count
    # #=> {123 => 4, 124 => 1}
    #
    def self.implicit_ratings_count
      courses = {}
      implicit_rql = Rating.course_ratings.with_implicit_rating.to_rql
      NoBrainer.run{implicit_rql.group('course_id').count }.each {|data| courses[data[0]] = data[1]}

      courses
    end

    def ctv_course_url
      "#{App.params['web']['ctv_course_url']}/#{course_id}/#{title.parameterize}"
    end

  end
end