module Models
  class CourseReviewFeedback

    include NoBrainer::Document
    include NoBrainer::Document::Timestamps

    store_in :table => 'course_review_feedback'

    field :user_id,  :type => Integer, :required => true
    field :feedback, :type => Boolean, :required => true

    # Relations
    belongs_to :user,  :foreign_key => :user_id # the user who gives the feedback to the review
    belongs_to :course_review, :index => true

    scope :positive_feedback,  -> { where(:feedback.eq => true)}
    scope :negative_feedback,  -> { where(:feedback.eq => false)}


  end
end