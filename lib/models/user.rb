module Models

  ##
  #
  #

  class User 
    include NoBrainer::Document
    include NoBrainer::Document::Timestamps
    store_in :table => 'users'
      
    field :user_id,     :type => Integer, :required => true, :primary_key => true, :unique => true, :index => true
    field :email,       :type => String,  :validates => { :format => { :with => /@/ } }, :index => true
    field :username,    :type => String
    field :first_name,  :type => String
    field :last_name,   :type => String
    field :language,    :type => String
    field :country,     :type => String
    field :gender,      :type => String

    #others:
    field :is_email_subscriber, :type => Boolean, :default => true

    ## Relations:
    has_many :ratings, :foreign_key => :user_id
    has_many :course_ratings,  :foreign_key => :user_id, :class_name => 'Rating', :scope => ->{course_ratings}
    has_many :lecture_ratings, :foreign_key => :user_id, :class_name => 'Rating', :scope => ->{lecture_ratings}

    ##
    # Creates or updates the Lectures models from the Classroom.tv view_spanish_language_users view.

    def self.load_users_from_classroomtv

        documents = []
        query = 'SELECT id user_id, email, username, first_name, last_name, language, country, gender FROM view_spa_users'

        log = {
            total:   0,
            created: 0,
            updated: 0,
            failed:  0
        }

        Classroomtv::mysql_client.query(query, :stream => true, :symbolize_keys => true).each do |row|

          user = Models::User.where(:user_id => row[:user_id]).first

          if user.nil?

            row[:created_at] = Time.now
            row[:updated_at] = Time.now

            if documents.push(row).count == 200
              log[:created] += Models::User.insert_all(documents).count
              documents = []
            end
          else
            row.delete(:user_id)
            user.assign_attributes(row)
            log[user.save ? :updated : :failed] += 1 if user.changed?
          end

          log[:total] += 1

        end

        unless documents.empty?
          log[:created] += Models::User.insert_all(documents).count unless documents.empty?
        end 

        log
    end

    def hi_name
      first_name.nil? ? username : "#{first_name} #{last_name}"
    end

    def email_subscriber?
      !!is_email_subscriber
    end

    ##
    # intended to encrypt the user data, like the user_id
    #
    def self.encrypt(data)
      cipher(:encrypt, data.to_s)
    end

    def self.decrypt(data)
      cipher(:decrypt, data.to_s)
    end

    private

    def self.cipher(mode, data)
      cipher = OpenSSL::Cipher::Cipher.new('bf-cbc').send(mode)
      cipher.key = Digest::SHA256.digest(ENV['ENCRYPT_KEY'] || '2f66f16372c2ebdb21d02bf380db23f7')
      cipher.update(data) << cipher.final
    end

  end
  
end
