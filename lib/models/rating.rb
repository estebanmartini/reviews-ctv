module Models
  ##
  # This class represents the ClassroomTV Lecture model, it is used as local copy to decouple the systems,
  # all its entries must exist in the Classroom.tv database and should not be created manually
  #
  class Rating
    include NoBrainer::Document
    include NoBrainer::Document::Timestamps

    store_in :table => 'ratings'

    field :explicit_rating, :type => Integer
    field :implicit_rating, :type => Integer
    field :original_value,  :type => Float

    field :is_enrolled_to_course, :type => Boolean    # if the user is enrolled to course
    field :is_favorite_lecture,   :type => Boolean    # if the user marked lecture as favorite

    ## Indexes
    index :user_course,  [:user_id, :course_id]
    index :user_lecture, [:user_id, :lecture_id]

    ## Relations
    belongs_to :user,     :foreign_key => :user_id, :required => true, :index => true
    belongs_to :course,   :foreign_key => :course_id, :index => true
    belongs_to :lecture,  :foreign_key => :lecture_id
    has_one :course_review

    ## Scopes
    scope :course_ratings,  -> { where(:course_id.defined => true)}
    scope :lecture_ratings, -> { where(:lecture_id.defined => true)}
    scope :with_explicit_rating, -> {where(:explicit_rating.gt => 0)}
    scope :with_implicit_rating, -> {
      where(:or =>[{:implicit_rating.eq => 1 }, {:is_enrolled_to_course.eq => true}])
    }

    scope :courses_with_explicit_rating, -> {course_ratings.with_explicit_rating}
    scope :courses_with_implicit_rating, -> {course_ratings.with_implicit_rating}
    scope :lectures_with_implicit_rating, -> {lecture_ratings.with_implicit_rating}


    ## Callbacks
    after_save    :update_course_rating
    after_destroy :update_course_rating

    ## the lower threshold to consider a lecture or course as watched
    WATCHED_THRESHOLD = 20

    ##
    # Creates or updates the Lectures models from Classroom.tv database.
    #
    def self.load_ratings_from_classroomtv(item_type)

      log = {
          total:   0,
          created: 0,
          updated: 0,
          failed:  0
      }
      documents = []

      if item_type == 'course'
        query = 'SELECT course_id,user_id,implicit_rating,original_value, is_enrolled_to_course FROM view_spa_user_course_ratings'
      else
        query = 'SELECT lecture_id,user_id,implicit_rating,original_value FROM view_spa_user_lecture_ratings'
      end

      Classroomtv::mysql_client.query(query, :stream => true, :symbolize_keys => true).each do |row|

        conditions = item_type == 'lecture' ? {:lecture_id => row[:lecture_id]} : {:course_id => row[:course_id]}
        conditions[:user_id] = row[:user_id]

        rating = Models::Rating.where(conditions).first

        if rating.nil?

          row[:created_at] = Time.now
          row[:updated_at] = Time.now

          if documents.push(row).size == 200
            log[:created] += Models::Rating.insert_all(documents).count
            documents = []
          end
        else
          log[rating.update(row) ? :updated : :failed] += 1 if rating.changed?
        end

        log[:total] += 1

      end

      log[:created] += Models::Rating.insert_all(documents).size unless documents.empty?

      # update the course average ratings
      if item_type == 'course'
        course_ids = {}

        Rating.course_ratings.with_explicit_rating.each do |c|
          if course_ids[c.course_id].nil?
            Course.update_aggregated_fields(c.course_id)
            course_ids[c.course_id] = c.course_id
          end
        end

      end

      log
    end

    ##
    #
    #

    def set_implicit_rating_from_threshold
      self.implicit_rating ||= self.original_value >= WATCHED_THRESHOLD ? 1 : 0
    end

    ##
    #
    # Returns a Hash

    def self.courses_stats

      stats = {}

      ## Explicit Rating
      explicit_scope = Rating.course_ratings.with_explicit_rating
      stats[:explicit_rating] = {
          :count          => NoBrainer.run {explicit_scope.to_rql.count},
          :unique_users   => NoBrainer.run {explicit_scope.to_rql.group('user_id').count}.count,
          :unique_courses => NoBrainer.run {explicit_scope.to_rql.group('course_id').count}.count,
          :reviews_count  => CourseReview.count
      }

      stats[:implicit_rating] = {
          :count => Rating.course_ratings.with_implicit_rating.count
      }

      stats
    end

    ##
    # Updates the course average_rating and rated_by_count
    #

    def update_course_rating

      Course.update_aggregated_fields(course_id) unless course_id.nil?

    end

    def update_ctv_course_ratings
      uri = URI.parse("#{App.params['web']['update_course_ratings_url']}/#{course_id}")
      pp Net::HTTP.post_form(uri, {'rating' => course.average_rating, 'ratings_count' => course.explicit_ratings_count})
    end

    def self.update_ctv_course_ratings
      Models::Rating.courses_with_explicit_rating.each do |r|
        r.update_ctv_course_ratings
      end
    end

  end
end