module Models
  class RecommendationSet

    include NoBrainer::Document
    include NoBrainer::Document::Timestamps

    store_in :table => 'recommendation_set'

    field :rating_type, :type => Integer
    field :item_type,   :type => Integer
    field :positive_feedback, :type => Boolean

    belongs_to :user, :foreign_key => :user_id, :index => true
    has_many   :recommended_items

    TYPE_IMPLICIT = 1
    TYPE_EXPLICIT = 2

    ITEM_TYPE_COURSE  = 1
    ITEM_TYPE_LECTURE = 2

    scope :with_explicit_rating, -> {where(:rating_type.eq => TYPE_EXPLICIT)}
    scope :with_implicit_rating, -> {where(:rating_type.eq => TYPE_IMPLICIT)}

    scope :with_lecture_type, -> {where(:item_type.eq => ITEM_TYPE_LECTURE)}
    scope :with_course_type,  -> {where(:item_type.eq => ITEM_TYPE_COURSE)}

    ##
    #
    #
    def create_recommended_items(item_type, recommended_items)
      recommended_items.each do |rec|

        if item_type == ITEM_TYPE_COURSE
          item = RecommendedCourse.new(course_id: rec['item'])
        else
          item = RecommendedLecture.new(lecture_id: rec['item'])
        end

        item.prediction_score = rec['score']
        item.recommendation_set_id = self.id

        item.save!
      end
    end

    ##
    #
    #
    def update_recommended_items(item_type, recommended_items)

      rec_items = recommended_items.inject({}){|result, rec| result[rec['item'].to_s] = rec['score']; result}

      self.recommended_items.each do |item|

        key = (item.course_id || item.lecture_id).to_s

        unless rec_items[key].nil?

          if item.prediction_score != rec_items[key]
            item.previous_ratings = [] if item.previous_ratings.nil?
            item.previous_ratings << item.prediction_score
            item.prediction_score = rec_items[key]
            item.save
          end

          rec_items.delete(key)

        end

      end

      rec_items.each do |item_id, score|

        if item_type == ITEM_TYPE_COURSE
          item = RecommendedCourse.new(course_id: item_id)
        else
          item = RecommendedLecture.new(lecture_id: item_id)
        end

        item.prediction_score = score
        item.recommendation_set_id = id

        item.save!
      end

    end

    ##
    # Gets the top recommended items that user has not rated yet
    #
    def recommended_not_rated_items(limit=100)

      item_label = ''
      item_ids = []

      if item_type == ITEM_TYPE_COURSE

        item_label = 'course_id'
        user_ratings = Rating.where(:user_id=>user_id)

        if rating_type == TYPE_IMPLICIT
          item_ids = user_ratings.courses_with_implicit_rating.map{|r| r.course_id}
        else
          item_ids = user_ratings.courses_with_explicit_rating.map{|r| r.course_id}
        end

      else
        item_ids = user_ratings.lectures_with_implicit_rating.map{|r| r.lecture_id}
        item_label = 'lecture_id'
      end

      #NoBrainer.run{|r| recommended_items.to_rql.filter{ |doc| r.expr(item_ids).contains(doc[item_label]).not }.limit(limit) }

      rec_ids = recommended_items.map{|item| item.send(item_label).to_i}

      rec_items_ids = rec_ids - item_ids

      rec_items_ids[0, limit]


    end

  end
end