module Models
  class RecommendedItem
    include NoBrainer::Document
    include NoBrainer::Document::Timestamps

    store_in :table => 'recommended_items'

    field :prediction_score,  :type => Float
    field :positive_feedback, :type => Boolean
    field :previous_ratings,  :type => Array

    belongs_to :recommendation_set

    default_scope { order_by(:prediction_score => :desc) }
  end
end
