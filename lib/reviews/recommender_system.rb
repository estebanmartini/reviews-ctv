module Reviews
  class RecommenderSystem

    @client

    def initialize(access_key, eventserver_url = 'http://localhost:7070')
      @client = PredictionIO::EventClient.new(access_key,eventserver_url)
    end

    ##
    # Intended for the implicit ratings RS
    #
    def create_event(rating)

      entity = {
          'targetEntityType' => 'item',
          'targetEntityId'   => rating.course_id || rating.lecture_id
      }

      if rating.is_enrolled_to_course
        @client.create_event('enroll', 'user', rating.user_id, entity)
      end

      if rating.implicit_rating
        @client.create_event('view', 'user', rating.user_id, entity)
      end

    end

    ##
    # Intended for explicit ratings
    def create_event_rate(rating)

      if !rating.explicit_rating.nil? && rating.explicit_rating > 0
        rate_value = rating.explicit_rating

        entity = {
            'targetEntityType' =>  'item',
            'targetEntityId' => rating.course_id,
            'properties' => {'rating' =>  rate_value}
        }

        @client.create_event('rate', 'user', rating.user_id, entity)

      else
        #select the max value between enrollment or course progress
        #progress_rating = (rating.original_value / 20.0).ceil
        #rate_value = [(rating.is_enrolled_to_course ? 1 : 0)*3, progress_rating].max
      end

    end

    def create_event_user(user_id)
      @client.create_event('$set', 'user', user_id)
    end

    def create_event_item(item_id)
      properties = {} #categories...
      @client.create_event('$set', 'item', item_id, properties)
    end

    ##
    # It will create the EventServer events according the given
    #
    # Returns and array with errors if they occurred
    def create_events(item_type, type)
      errors = []

      ratings = Models::Rating.course_ratings

      if item_type == Models::RecommendationSet::ITEM_TYPE_LECTURE
        ratings = Models::Rating.lecture_ratings
      end

      if type == Models::RecommendationSet::TYPE_IMPLICIT
        ratings.with_implicit_rating.each do |r|
          begin
            create_event(r)
          rescue Exception => e
            errors << e.message
          end
        end
      else
        explicit_scope = ratings.with_explicit_rating

        users   = NoBrainer.run {explicit_scope.to_rql.group('user_id').count}
        courses = NoBrainer.run {explicit_scope.to_rql.group('course_id').count}

        users.each{|u| create_event_user(u[0])}        # add the items
        courses.each{|c| create_event_item(c[0])}      # add the courses
        explicit_scope.each{|r| create_event_rate(r)}  # add the ratings

      end
    end

    ##
    # Creates the recommended sets for all the users, this method should be
    # executed after each PredictionIO train call
    #
    def create_recommendation_sets(item_type, rating_type, deploy_url='http://localhost:8000')
      errors     = []
      pio_engine = PredictionIO::EngineClient.new(deploy_url)

      users = Models::Rating.course_ratings

      if item_type == Models::RecommendationSet::ITEM_TYPE_LECTURE
        users = Models::Rating.lecture_ratings
      end

      if rating_type == Models::RecommendationSet::TYPE_IMPLICIT
        users = users.with_implicit_rating
      else
        users = users.with_explicit_rating
      end

      NoBrainer::run {users.to_rql.group('user_id').count}.each do |u|
        begin
          user_id = u[0]

          num = 10 + users.where(:user_id=>user_id).count

          recommendations = pio_engine.send_query('user'=>user_id, 'num'=>num)['itemScores']

          if recommendations.count > 0

            rec_set_data = {
                    user_id: user_id,
                  item_type: item_type,
                rating_type: rating_type
            }

            rec_set = Models::RecommendationSet.where(rec_set_data).first

            if rec_set.nil?
              rec_set = Models::RecommendationSet.create(rec_set_data)
              rec_set.create_recommended_items(item_type, recommendations)
            else
              rec_set.update_recommended_items(item_type, recommendations)
            end

          end

        rescue Exception => e
          errors << e.message
        end
      end
      pp errors unless errors.empty?
    end

  end
end