(function () {
    var app = angular.module('course_reviews', ['ngAnimate', 'angular-loading-bar']);

    app.controller('FooterController', ['$scope', function ($scope) {

        $scope.scrollTo = function(element, to, duration) {
            if (duration < 0) return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;

            setTimeout(function() {
                element.scrollTop = element.scrollTop + perTick;
                $scope.scrollTo(element, to, duration - 2);
            }, 10);
        };

        $scope.backTop = function () {
            var el = document.body.scrollTop !== 0 ? document.body : document.body.parentNode;

            $scope.scrollTo(el, 0, 80);
        };
    }]);

    app.controller('CourseReviewsController', ['$http', '$scope', function ($http, $scope) {

        var courseReview = this;
        this.courses = [];

        $scope.init = function (userId) {
            $http({
                url: '/api/v1/courses/ratings',
                method: 'GET',
                params: {user_id: userId}
            }).success(function (data) {
                courseReview.courses = data;
            });
        };
    }]);

    app.controller("ReviewController", ['$http', '$log', '$scope', function ($http, $log, $scope) {

        $scope.showCourseReviewForm = false;
        $scope.isModelNew = true;
        $scope.savingForm = false;
        $scope.ratingUpdated = false;

        $scope.showTheCourseReviewForm = function (courseId) {
            $scope.showCourseReviewForm = true;
        };

        $scope.saveReview = function (course) {

            var review = course.rating.review,
                url = '/api/v1';

            if (review.rating_id === undefined) {
                review.rating_id = course.rating.id;
            }

            if (!review.id) {
                url += '/ratings/' +  review.rating_id + '/reviews';
            } else {
                url += '/reviews/' + review.id
            }

            $scope.savingForm = true;


            $http({
                url: url,
                method: review.id ? 'PUT' : 'POST',
                data: {review: review}
            }).success(function (data) {
                course.rating.review = data;
                $scope.isModelNew = false;
                $scope.showCourseReviewForm = false;
            }).error(function (data) {
                alert('Ocurrió un error guardando tu comentario, lo sentimos :(');
            }).finally(function () {
                $scope.savingForm = false;
            });

        };

        $scope.updateExplicitRating = function (rating) {
            $scope.ratingUpdated = false;

            $http({
                url: '/api/v1/ratings/' +  rating.id,
                method: 'PUT',
                data: {rating: rating}
            }).success(function (data) {
                $scope.ratingUpdated = true;
            }).error(function (data) {
                alert('Ocurrió un error guardando tu calificación, lo sentimos :(')
            });
        }

    }]);

    app.directive('courseReviewForm', function () {
        return {
            restrict: 'E',
            templateUrl: '/js/apps/users/courses/reviews/course-review-form.html'
        }
    });

    app.filter('nospace', function () {
        return function (value) {
            return (!value) ? '' : value.replace(/ /g, '-');
        };
    });

    app.directive('starRating', function() {
        return {
            restrict: 'E',
            templateUrl: '/js/apps/users/courses/reviews/star-rating.html',
            scope : {
                rating : "=ngModel",
                courseRating : "=courseRating",
                onRatingSelected : "&?"
            },
            link : function(scope, elem, attrs) {
                scope.rating = scope.rating || 0;

                var updateStars = function () {
                    if (!scope.stars) {
                        scope.stars = [
                            {label: 'Muy malo'},
                            {label: 'Malo'},
                            {label: 'Está bien'},
                            {label: 'Me gusta'},
                            {label: 'Me encanta'}
                        ]
                    }

                    for (var i = 0; i < 5; i++) {
                        scope.stars[i].filled = i < scope.rating
                    }
                };

                updateStars();

                scope.toggle = function(index) {
                    scope.rating = index + 1;
                    scope.courseRating.explicit_rating = scope.rating;
                    scope.onRatingSelected(scope.courseRating);
                    scope.stars[index].filled = true;
                };

                scope.mouserOver = function (index, el) {
                    scope.ratingLabel = scope.stars[index].label;
                };

                scope.setRatingLabel = function () {
                    scope.ratingLabel = scope.rating > 0 ? scope.stars[scope.rating-1].label : '';
                };

                scope.setRatingLabel();
                scope.$watch("rating", updateStars);
            }
        };
    });

    app.directive('syncFocusWith', function($timeout, $rootScope) {
        return {
            restrict: 'A',
            scope: {
                focusValue: "=syncFocusWith"
            },
            link: function($scope, $element, attrs) {
                $scope.$watch("focusValue", function(currentValue, previousValue) {
                    if (currentValue === true && !previousValue) {
                        $timeout(function () {
                            $element[0].focus();
                        });
                    }
                })
            }
        }
    });

})();