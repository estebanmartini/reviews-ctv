(function () {
    var app = angular.module('email_subscription', ['ngAnimate', 'angular-loading-bar']);

    app.controller('EmailSubscriberController', ['$scope','$http', function ($scope,$http) {

        $scope.user = {};
        $scope.showSuccess = false;

        $scope.init = function (userId, isEmailSubscriber) {
            $scope.user = {
                id: userId,
                is_email_subscriber: isEmailSubscriber
            };

            $scope.showSuccess = isEmailSubscriber !== true;
        };

        $scope.unsubscribe = function () {

            $scope.user.is_email_subscriber = false;

            $http({
                url: '/api/v1/users/' +  $scope.user.id,
                method: 'PUT',
                data: {user: {is_email_subscriber: $scope.user.is_email_subscriber}}
            }).success(function (data) {
                $scope.showSuccess = true;
            }).error(function (data) {
                alert('Ocurrió un error guardando tu calificación, lo sentimos :(')
            });
        };
    }]);

    app.controller('FooterController', ['$scope', function ($scope) {

    }]);

})();