require 'rake/testtask'

## test
Rake::TestTask.new do |task|
  task.libs << %w(spec lib apps)
  task.pattern = 'spec/*/spec_*.rb'
end

## test-models
Rake::TestTask.new do |task|
  task.name = 'test:models'
  task.libs << %w(spec lib)
  task.pattern = 'spec/models/spec_*.rb'
end

## test-api
Rake::TestTask.new do |task|
  task.name ='test:api'
  task.libs << %w(spec lib apps)
  task.pattern = 'spec/api/spec_*.rb'
end

## nobrainer
namespace :nobrainer do

  task :environment do
    require_relative 'lib/reviews_classroomtv'
  end

  desc 'Synchronize index definitions'
  task :sync_indexes => :environment do
    NoBrainer.sync_indexes(:verbose => true)
  end

  task :sync_indexes_quiet => :environment do
    NoBrainer.sync_indexes
  end

  desc 'Drop the database'
  task :drop => :environment do
    NoBrainer.drop!
  end

  desc 'Drop the database + Sync index definitions'
  task :reset => [:drop, :sync_indexes_quiet]

end

## Syncs the data from Classroomtv
namespace :sync_classroomtv_data do
  task :environment do
    require_relative 'lib/reviews_classroomtv'
  end

  task :users => :environment do
    pp 'users: '
    pp Models::User.load_users_from_classroomtv
  end

  task :courses => :environment do
    pp 'courses: '
    pp Models::Course.load_courses_from_classroomtv
  end

  task :lectures => :environment do
    pp 'lectures: '
    pp Models::Lecture.load_lectures_from_classroomtv
  end

  task :lecture_ratings => :environment do
    pp 'lecture ratings: '
    pp Models::Rating.load_ratings_from_classroomtv('lecture')
  end

  task :course_ratings  => :environment do
    pp 'course ratings: '
    pp Models::Rating.load_ratings_from_classroomtv('course')
  end

  task :all => [:users, :courses, :lectures, :lecture_ratings, :course_ratings]

  ##
  #
  #
  task :update_ratings => :environment do
    Models::Rating.update_ctv_course_ratings
  end

end

namespace :mailing do

  task :environment do
    require_relative 'lib/reviews_classroomtv'
  end

  task :send_all_recommended_courses => :environment do
    UserMailer::send_all_recommended_courses(!!ENV['is_test'])
  end

  task :send_all_rate_courses => :environment do
    pp UserMailer.send_all_rate_courses(!!ENV['is_test'])
  end

  task :test => :environment do
    u = Models::Rating.course_ratings.where(:user_id.eq => 4397).first
    p u.user.email
    UserMailer.new.rate_courses(u.user_id)
  end

end

task :environment do
  require_relative 'lib/reviews_classroomtv'
end

task :auth_token => :environment do
  user_id = ENV['user_id']
  puts Base64.urlsafe_encode64(Models::User.encrypt(user_id.to_i))
end

##
# Recommender System tasks
#
#
namespace :rs do

  task :environment do
    require_relative 'lib/reviews_classroomtv'
  end

  ##
  # Examples:
  # rake rs:create_events rating=implicit item=lecture
  #
  task :create_events => :environment do

    if ENV['rating'].downcase == 'explicit' && ENV['item'] == 'lecture'
      raise(Exception.new('Lectures have not explicit ratings'))
    end

    app_id      = ENV["PIO_#{ENV['item'].upcase}_#{ENV['rating'].upcase}_ID"]
    item_type   = Models::RecommendationSet::ITEM_TYPE_LECTURE
    rating_type = Models::RecommendationSet::TYPE_EXPLICIT

    if ENV['item'].downcase == 'course'
      item_type = Models::RecommendationSet::ITEM_TYPE_COURSE
    end

    if ENV['rating'].downcase == 'implicit'
      rating_type = Models::RecommendationSet::TYPE_IMPLICIT
    end

    Reviews::RecommenderSystem.new(app_id).create_events(item_type, rating_type)
  end

  ##
  #
  # Examples:
  # rake rs:create_recommendations_sets rating=implicit item=lecture
  #
  task :create_recommendations_sets => :environment do

    if ENV['rating'] == 'explicit' && ENV['item'] == 'lecture'
      raise(Exception.new('Lectures have not explicit ratings'))
    end

    app_id      = ENV["PIO_#{ENV['item'].upcase}_#{ENV['rating'].upcase}_ID"]
    item_type   = Models::RecommendationSet::ITEM_TYPE_LECTURE
    rating_type = Models::RecommendationSet::TYPE_EXPLICIT

    if ENV['item'].downcase == 'course'
      item_type = Models::RecommendationSet::ITEM_TYPE_COURSE
    end

    if ENV['rating'].downcase == 'implicit'
      rating_type = Models::RecommendationSet::TYPE_IMPLICIT
    end

    Reviews::RecommenderSystem.new(app_id).create_recommendation_sets(item_type, rating_type)
  end

end