Reviews.Classroom.tv
=====================

Author: Esteban Martini

# Rake Tasks

## nobrainer

```
# Synchronizes the Rethink indexes
rake nobrainer:sync_indexes        

# Synchronizes the Rethink indexes quietly
rake nobrainer:sync_indexes_quiet  

# Drops the environment database and sync the indexes
rake nobrainer:reset               
```

## test

```
# Runs all the spec tests
rake test

# Runs all the model tests
rake test:models

# Runs all the spec tests
rake test:api
```

## classroomtv

```
# Synchronizes all classroomtv data into the reviews system
rake sync_classroomtv_data:all
```
