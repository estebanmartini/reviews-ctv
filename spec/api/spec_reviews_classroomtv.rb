require 'api_helper'

describe Apps::API do

  describe '/api/v1/ratings' do

    it 'get when the rating exists' do
      rating = create(:explicit_course_rating)

      get "/v1/ratings/#{rating.id}"
      last_response.ok?.must_equal true

      response = JSON.parse(last_response.body)
      response['id'].must_equal(rating.id)
      response['user_id'].must_equal(rating.user_id)
      response['course_id'].must_equal(rating.course_id)

    end

    it 'get /:id when the rating does not exists' do

      get '/v1/ratings/098765432'
      last_response.ok?.must_equal false
      last_response.status.must_equal 404
    end

    it 'post /:id' do
      attributes = build(:explicit_course_rating).attributes
      post '/v1/ratings', {:rating => attributes}

      last_response.status.must_equal 201 #wow!
    end

    it 'post /ratings when required params are missing' do
      post '/v1/ratings', {:rating => {}}

      last_response.status.must_equal 400
    end

    it 'post /ratings when required course and don´t exist' do
      post '/v1/ratings', {:rating => {course_id: 123, user_id: 4499, explicit_rating: 9}}
      last_response.status.must_equal 400
      response = JSON.parse(last_response.body,{:symbolize_names => true})
      response[:error][:user].wont_be_nil
      response[:error][:course_id].wont_be_nil
    end
  end

  describe '/courses' do

    it 'get user courses' do
      rating = create(:implicit_course_rating)
      get '/v1/courses', {:user_id => rating.user_id}
      last_response.ok?.must_equal false
    end

  end

end
