ENV['ENV'] = 'test'

require 'minitest/spec'
require 'minitest/autorun'
require "minitest/reporters"
require 'faker'
require 'factory_girl'
require 'reviews_classroomtv'

## force the load of factories
FactoryGirl.find_definitions

## use a spec like reporter
Minitest::Reporters.use! [Minitest::Reporters::SpecReporter.new]

class MiniTest::Spec
  include FactoryGirl::Syntax::Methods

  before :each do
    NoBrainer.purge!
    FactoryGirl.lint
  end

  after :each do
    #fixtures or something...
  end
end

