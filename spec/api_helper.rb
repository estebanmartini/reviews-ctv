##
# Extends the spec_helper to run tests against the API

require 'grape'
require 'rack/test'
require 'hashie-forbidden_attributes'

require 'spec_helper'
require 'api'

ENV['RACK_ENV'] = 'test'

## extend the MiniTest::Spec class adding the rack test methods
class MiniTest::Spec
  include Rack::Test::Methods

  def app
    Apps::API
  end

end
