require 'spec_helper'

include Models

describe Models do

  describe Rating do

    it 'create a implicit rating for course' do
      rating = build(:implicit_course_rating)

      rating.save.must_equal true
      rating.course.wont_be_nil
      rating.lecture.must_be_nil
    end

    it 'create a implicit rating for lecture' do
      rating = build(:implicit_lecture_rating)

      rating.save.must_equal true
      rating.course.must_be_nil
      rating.lecture.wont_be_nil
    end

    it 'can´t create a rating from an inexisting course and user' do

      rating = Rating.new({
        :course_id => 12345678910,
        :lecture_id => create(:lecture).lecture_id,
        :explicit_rating => 4
      })

      rating.save.must_equal false
      rating.errors[:user_id].wont_be_nil
      rating.errors[:course_id].wont_be_nil
    end

  end


  describe CourseReview do

    subject { create(:course_review) }
    specify { subject.rating.wont_be_nil }
    specify { subject.rating.user.wont_be_nil }
    specify { subject.rating.course.wont_be_nil }

  end

  describe CourseReviewFeedback do

    subject { create(:course_review_feedback) }
    specify { subject.user.wont_be_nil }
    specify { subject.course_review.rating.wont_be_nil }
    specify { subject.course_review.rating.user.wont_be_nil }
    specify { subject.course_review.rating.course.wont_be_nil }

  end

  describe User do

    subject { create(:user) }
    specify { subject.persisted?.must_equal true}

    it '.course_ratings' do
      user = create(:user_with_course_ratings, ratings_count: 15)
      user.course_ratings.count.must_equal 15
    end

    it '.lecture_ratings' do
      count = 11
      user = create(:user_with_lecture_ratings, ratings_count: count)
      user.lecture_ratings.count.must_equal count
    end

  end

  describe Course do

    it 'automatically sets the course average rating' do
      course = create(:course)
      course_ratings = create_list(:explicit_course_rating, 25, {course: course, explicit_rating: 1})
      course.reload

      course.average_rating.must_equal 1
      course.explicit_ratings_count.must_equal 25

      course_ratings.first.destroy
      course.reload

      course.average_rating.must_equal 1
      course.explicit_ratings_count.must_equal 24

    end

  end


end
