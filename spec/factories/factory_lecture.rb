FactoryGirl.define do

  factory :lecture, :class => Models::Lecture do
    sequence(:lecture_id)
    title {Faker::Lorem.sentence(3)}
    view_count {Faker::Number.number(4).to_i}

    after(:build) do |lecture|
      lecture.course ||= FactoryGirl.create(:course)
    end

  end



end