FactoryGirl.define do

  factory :course_review_feedback, :class => Models::CourseReviewFeedback do

    feedback {[true, false, 0, 1].sample}

    association :course_review, factory: :course_review
    association :user, factory: :user

  end

end