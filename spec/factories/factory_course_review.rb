FactoryGirl.define do

  factory :course_review, :class => Models::CourseReview do

    title {Faker::Lorem.sentence(3)}
    body  {Faker::Lorem.paragraph(2)}

    after(:build) do |course_review|
      course_review.rating ||= FactoryGirl.create(:explicit_course_rating)
    end

  end

end