FactoryGirl.define do

  factory :user, :class => Models::User do
    user_id     {Faker::Number.number(8).to_i}
    email       {Faker::Internet.email}
    username    {Faker::Internet.user_name}
    first_name  {Faker::Name.first_name}
    last_name   {Faker::Name.last_name}
    language    'es'
    country     'Chile'
    gender      ['female','male'].sample


    factory :user_with_course_ratings, :class => Models::User  do

      transient do
        ratings_count 5
      end

      after(:create) do |user, evaluator|
        create_list(:explicit_course_rating, evaluator.ratings_count, user: user)
      end

    end

    factory :user_with_lecture_ratings, :class => Models::User  do

      transient do
        ratings_count 5
      end

      after(:create) do |user, evaluator|
        create_list(:implicit_lecture_rating, evaluator.ratings_count, user: user)
      end

    end

  end

end