FactoryGirl.define do

  factory :course, :class => Models::Course do
    sequence(:course_id)
    title {Faker::Lorem.sentence(3)}
    view_count {Faker::Number.number(4).to_i}
  end



end