FactoryGirl.define do

  factory :rating, :class => Models::Rating do

    after(:build) do |rating|
      rating.user ||= FactoryGirl.create(:user)
    end

    factory :implicit_course_rating, :class => Models::Rating do
      original_value {rand(0.0 ..100.0)}

      after(:build) do |rating|
        rating.implicit_rating = rating.set_implicit_rating_from_threshold
        rating.course ||= FactoryGirl.create(:course)
      end

    end


    factory :implicit_lecture_rating, :class => Models::Rating do
      original_value {rand(0.0..100.0)}

      after(:build) do |rating|
        rating.implicit_rating = rating.set_implicit_rating_from_threshold
        rating.lecture ||= FactoryGirl.create(:lecture)
      end

    end

    factory :explicit_course_rating, :class => Models::Rating do
      explicit_rating {rand(1..5)}

      after(:build) do |rating|
        rating.course ||= FactoryGirl.create(:course)
      end

    end

  end


end